Όνοματεπώνυμο: Αντώνης Σκαρλάτος
Α.Μ: sdi1400184


Υπάρχουν τρεις διαφορετικές ομάδες αρχείων για κάθε ένα από τα mirrorInitiator, mirrorServer και contentServer.
Τα αρχεία τύπου *_func.cpp υλοποιούν τις συναρτήσεις κάθε κατηγορίας.
Όταν ο mirrorInitiator ζητήσει έναν φάκελο, τότε αρχίζει να ψάχνει ο contentServer από το path που παρέχει.
Συγκεκριμένα, αν θέλει να πάρει ο χρήστης όλα τα αρχεία που παρέχει ο contentServer θα πρέπει να ζητήσει την '.'.
Αν υπάρχει ένας υποφάκελος nested, τότε απλά ζητάει nested.
Ακόμα όταν αρχίζει ο MirroServer από την αρχή θα πρέπει να διαγράφεται το dirname του(πχ. rm -rf folder) για να μπορεί να το ξαναδημιουργήσει.
Σε αντίθετη περίπτωση θα πετάξει σφάλμα.


Compile & Run:
    make MirrorServer
    ./MirrorServer -p 2009 -m folder -w 3

    make ContentServer
    ./ContentServer -p 2010 -d folder_test1

    make MirrorInitiator
    ./MirrorInitiator -n localhost -p 2009 -s localhost:2010:.:2
