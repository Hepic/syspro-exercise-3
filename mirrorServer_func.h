#ifndef MIRROR_SERVER_FUNC_H
#define MIRROR_SERVER_FUNC_H

#define BUFFER_SIZE 150

struct ContentServersInfo
{
    char *address, *dirfilename;
    int port, delay, id, pos, len;
    
    ContentServersInfo();
    ~ContentServersInfo();
    void insert(char*, char*, int, int, int, int);
    void print() const;
};

struct BufferInfo
{
    ContentServersInfo data[BUFFER_SIZE+5];
    int count, Tbeg, Tend;

    BufferInfo();
};

extern BufferInfo buffer;
extern pthread_mutex_t mtx, mtx_filesTransfered, mtx_bytesTransfered, mtx_numDevicesDone;
extern pthread_cond_t cond_non_empty, cond_non_full, cond_all_done;
extern const char *dirname;
extern long int filesTransfered, numDevicesDone;
extern double bytesTransfered;

void *workersProcedure(void*);
void *mirrorManagerProcedure(void*);
void createDir();
bool sendIsDir();
void fetchFilename();
ContentServersInfo* parseContentServersInfo(const char*, int);
int getContentServersLength(const char*);
void sendBytesOfEachFile(const char *);
void parseSingleContentServerInfo(ContentServersInfo&, const char*);
void sendLengthBytesDir(const char*, int);
void sendLengthBytesEntity(const char*, struct dirent*, int);

#endif
