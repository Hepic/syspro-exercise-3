#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include "help_functions.h"
#include "contentServer_func.h"


int main(int argc, char *argv[])
{
    const char *content_server_port_str = parseWithFlag(argc, argv, "-p"); 
    dirfilename = parseWithFlag(argc, argv, "-d");
    int sock, new_sock, err, clone_size = CLONE_SIZE, clone_pos = 0;
    int content_server_port = convertStrToInt(content_server_port_str);
    sockaddr_in server, client;
    sockaddr *serverptr = (sockaddr*)&server;
    sockaddr *clientptr = (sockaddr*)&client;
    hostent *rem;
    socklen_t client_len = sizeof(client);
    pthread_t *clone; 
    
    pthread_mutex_init(&mtx, 0);
    
    if((clone = (pthread_t*)malloc(CLONE_SIZE*sizeof(pthread_t))) == NULL)
        perror_exit("malloc"); 

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(content_server_port);

    if(bind(sock, serverptr, sizeof(server)) < 0)
        perror_exit("bind");

    if(listen(sock, 5) < 0)
        perror_exit("listen");
    
    while(1)
    {
        if((new_sock = accept(sock, clientptr, &client_len)) < 0)
            perror_exit("accept");

        //socklen_t client_len = sizeof(client.sin_addr.s_addr);

        /*if((rem = gethostbyaddr((char*)&client.sin_addr.s_addr, client_len, client.sin_family)) == NULL)
            perror_exit("gethostbyaddr");

        printf("Came new connection from %s\n", rem->h_name);
        */
        
        if(err = pthread_create(clone+clone_pos, NULL, contentServerProcedure, (void*)&new_sock)) // start new thread per connection
            perror_pthread_exit("pthread_create", err);
        
        if(++clone_pos == clone_size)
        {
            clone_size += CLONE_SIZE;
            clone = (pthread_t*)realloc(clone, clone_size*sizeof(pthread_t));
        }
    }
    
    close(sock);
    return 0;
}
