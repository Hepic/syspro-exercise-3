#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include "help_functions.h"
#include "mirrorServer_func.h"


BufferInfo buffer;
pthread_mutex_t mtx, mtx_filesTransfered, mtx_bytesTransfered, mtx_numDevicesDone;
pthread_cond_t cond_non_empty, cond_non_full, cond_all_done;
const char *dirname;
long int filesTransfered, numDevicesDone;
double bytesTransfered;


BufferInfo::BufferInfo()
{
    count = Tbeg = Tend = 0;
}


ContentServersInfo::ContentServersInfo()
{
    address = dirfilename = NULL;
    port = delay = pos = len = -1; 
}


ContentServersInfo::~ContentServersInfo()
{
    if(address != NULL)
        delete[] address;
    
    if(dirfilename != NULL)
        delete[] dirfilename;
}


void ContentServersInfo::insert(char *_address, char *_dirfilename, int _port, int _id, int _pos, int _len)
{
    address = new char[strlen(_address)+5];
    strcpy(address, _address);
    
    dirfilename = new char[strlen(_dirfilename)+5];
    strcpy(dirfilename, _dirfilename);

    port = _port;
    pos = _pos;
    len = _len;
    id = _id;
    delay = -1;
}


void ContentServersInfo::print() const
{
    printf("Address = %s, Port = %d, Dirfilename = %s, Delay = %d\n", address, port, dirfilename, delay); 
}


ContentServersInfo* parseContentServersInfo(const char *str, int len)
{
    ContentServersInfo *content_server_info = new ContentServersInfo[len+5];
    int pos = 0;

    char *temp = new char[strlen(str)+5];
    strcpy(temp, str);

    char *ptr = strtok(temp, ",");

    while(ptr != NULL)
    {
        parseSingleContentServerInfo(content_server_info[pos++], ptr);
        ptr = strtok(NULL, ",");
    }
    
    delete[] temp;
    return content_server_info;
}


void parseSingleContentServerInfo(ContentServersInfo &content_server, const char *str)
{
    char *temp = new char[strlen(str)+5];
    int pos = 0, cnt = 0;
    
    memset(temp, 0, (strlen(str)+5)*sizeof(char));
    
    for(int i=0; i<strlen(str); ++i)
    {
        if(str[i] == ':')
        {
            switch(++cnt)
            {
                case 1:
                    content_server.address = new char[pos+5];
                    memset(content_server.address, 0, (pos+5)*sizeof(char));
                    strcpy(content_server.address, temp);
                break;

                case 2:
                    content_server.port = convertStrToInt(temp);
                break;

                case 3:
                    content_server.dirfilename = new char[pos+5];
                    strcpy(content_server.dirfilename, temp);
                break;
            }
            
            pos = 0;
            memset(temp, 0, sizeof(char)*(strlen(str)+5));
            
            continue;
        }

        temp[pos++] = str[i];
    }

    content_server.delay = convertStrToInt(temp);
    delete[] temp;
}


int getContentServersLength(const char *str)
{
    if(str == NULL)
        return 0;

    int ret = 1;

    while(*str != '\0')
    {
        if(*str++ == ',')
            ++ret;        
    }

    return ret;
}


void sendLengthBytesDir(const char *path, int sock)
{
    DIR *dir;
    struct dirent *entity;
    
    if((dir = opendir(path)) == NULL)
        perror_exit("opendir");
    
    entity = readdir(dir);

    while(entity != NULL)
    {
        sendLengthBytesEntity(path, entity, sock);
        entity = readdir(dir);
    }

    closedir(dir);
}


void sendLengthBytesEntity(const char *_dirfilename, struct dirent *entity, int sock)
{
    if(entity->d_type == DT_DIR)
    {
        if(entity->d_name[0] == '.')
            return;
        
        int path_len = strlen(_dirfilename) + strlen(entity->d_name);
        char *path = new char[path_len+5];
        
        sprintf(path, "%s/%s", _dirfilename, entity->d_name);
        sendLengthBytesDir(path, sock);
        
        delete[] path;
    }

    else if(entity->d_type == DT_REG)
    {
        int path_len = strlen(_dirfilename) + strlen(entity->d_name) + 1;
        char *path = new char[path_len+5];

        sprintf(path, "%s/%s", _dirfilename, entity->d_name);
        double length = getFileSize(path);

        if(write(sock, &length, sizeof(length)) < 0)
            perror_exit("write");
        
        delete[] path;
    }

    else
        fprintf(stderr, "Not a file or directory\n");
}


void createDir()
{
    if(!strcmp(buffer.data[buffer.Tbeg].dirfilename, "./"))
        return;

    int path_len = strlen(dirname) + strlen(buffer.data[buffer.Tbeg].address) + 10 +
                   strlen(buffer.data[buffer.Tbeg].dirfilename);
    
    char *path = new char[path_len+5];
    
    memset(path, 0, (path_len+5)*sizeof(char));
    sprintf(path, "%s/%s_%d/%s", dirname, buffer.data[buffer.Tbeg].address, 
                                          buffer.data[buffer.Tbeg].port,
                                          buffer.data[buffer.Tbeg].dirfilename);
    
    if(!isDir(path)  &&  mkdir(path, 0777) < 0)
        perror_exit("mkdir");
    
    delete[] path;
}


bool sendIsDir()
{
    sockaddr_in server;
    sockaddr *serverptr = (sockaddr*)&server;
    hostent *rem; 
    int sock, path_len;
    char message[100];
    char *path;
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");
    
    if((rem = gethostbyname(buffer.data[buffer.Tbeg].address)) == NULL)
        perror_exit("gethostbyname");
    
    server.sin_family = AF_INET;
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_port = htons(buffer.data[buffer.Tbeg].port);
    
    if(connect(sock, serverptr, sizeof(server)) < 0) // connect to content_server 
        perror_exit("connect");
   
    //printf("(Worker) Connecting to %s:%d\n", buffer.data[buffer.Tbeg].address, buffer.data[buffer.Tbeg].port);

    memset(message, 0, sizeof(message));
    sprintf(message, "DIR %s", buffer.data[buffer.Tbeg].dirfilename);
    
    if(write(sock, message, sizeof(message)) < 0) // ask if it is directory
        perror_exit("write");
    
    memset(message, 0, sizeof(message));
    
    if(read(sock, message, sizeof(message)) < 0)
        perror_exit("read");
       
    close(sock);
    
    if(!strcmp(message, "yes"))
        return true;

    return false;
}


void fetchFilename()
{
    sockaddr_in server;
    sockaddr *serverptr = (sockaddr*)&server;
    hostent *rem; 
    int sock, path_len;
    char message[100];
    char *path;
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");
    
    if((rem = gethostbyname(buffer.data[buffer.Tbeg].address)) == NULL)
        perror_exit("gethostbyname");
    
    server.sin_family = AF_INET;
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_port = htons(buffer.data[buffer.Tbeg].port);
    
    if(connect(sock, serverptr, sizeof(server)) < 0) // connect to content_server 
        perror_exit("connect");
   
    //printf("(Worker) Connecting to %s:%d\n", buffer.data[buffer.Tbeg].address, buffer.data[buffer.Tbeg].port);

    memset(message, 0, sizeof(message));
    sprintf(message, "FETCH %d %s", buffer.data[buffer.Tbeg].id, buffer.data[buffer.Tbeg].dirfilename);
    
    if(write(sock, message, sizeof(message)) < 0) // send FETCH request
        perror_exit("write");
    
    path_len = strlen(dirname) + strlen(buffer.data[buffer.Tbeg].address) + 10;
    path_len += strlen(buffer.data[buffer.Tbeg].dirfilename);
    path = new char[path_len+5];
    
    sprintf(path, "%s/%s_%d/%s", dirname, buffer.data[buffer.Tbeg].address, buffer.data[buffer.Tbeg].port, 
                                 buffer.data[buffer.Tbeg].dirfilename);
    
    FILE *fp = fopen(path, "wb");
    int bytes_tosend = 0, bytes_read = 0, ret;
    
    while(1) // read bytes from the socket and write them in the file
    {
        if((ret = read(sock, &bytes_tosend, sizeof(bytes_tosend))) < 0)
            perror_exit("read");
       
        if(bytes_tosend == 0  ||  ret == 0)
            break;
        
        pthread_mutex_lock(&mtx_bytesTransfered);
        bytesTransfered += bytes_tosend;
        pthread_mutex_unlock(&mtx_bytesTransfered);

        while(bytes_tosend > 0)
        {
            memset(message, 0, sizeof(message));  

            if((bytes_read = read(sock, message, sizeof(message))) < 0)
                perror_exit("read");
            
            char *ptr = message;
            bytes_tosend -= bytes_read;

            while(bytes_read > 0)
            {
                int bytes_write = fwrite(ptr, sizeof(char), bytes_read, fp);
                bytes_read -= bytes_write;
                ptr += bytes_write;
            }
        }
    }
    
    pthread_mutex_lock(&mtx_filesTransfered);
    ++filesTransfered;
    pthread_mutex_unlock(&mtx_filesTransfered);
    
    if(buffer.data[buffer.Tbeg].pos == buffer.data[buffer.Tbeg].len) // last file of device
    {
        pthread_mutex_lock(&mtx_numDevicesDone);
        ++numDevicesDone;
        pthread_mutex_unlock(&mtx_numDevicesDone);
        pthread_cond_signal(&cond_all_done);
    }

    fclose(fp);
    close(sock);

    delete[] path;
}


void *workersProcedure(void *args)
{    
    while(1)
    {
        pthread_mutex_lock(&mtx);
    
        while(buffer.count <= 0)
        {
            //printf("Found Buffer empty\n");
            pthread_cond_wait(&cond_non_empty, &mtx);
        } 
        
        if(sendIsDir())
            createDir(); 
        else
            fetchFilename();
        
        buffer.Tbeg = (buffer.Tbeg + 1) % BUFFER_SIZE;
        --buffer.count;

        pthread_mutex_unlock(&mtx);
        pthread_cond_signal(&cond_non_full);
    }

    pthread_exit(NULL);
}


void *mirrorManagerProcedure(void *args)
{
    ContentServersInfo content_server_info = *((ContentServersInfo*)args); 
    sockaddr_in server;
    sockaddr *serverptr = (sockaddr*)&server;
    hostent *rem; 
    int sock, whole_len; 
    char message[100];

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");

    if((rem = gethostbyname(content_server_info.address)) == NULL)
        perror_exit("gethostbyname");
    
    server.sin_family = AF_INET;
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_port = htons(content_server_info.port);
    
    if(connect(sock, serverptr, sizeof(server)) < 0) // connect to content_server 
    {
        printf("(MirrorManager) Connecting to %s:%d failed\n", content_server_info.address, content_server_info.port);
        pthread_exit(NULL);
    }

    //printf("(MirrorManager) Connecting to %s:%d\n", content_server_info.address, content_server_info.port); 
    memset(message, 0, sizeof(message));
    sprintf(message, "LIST %d %d", content_server_info.id, content_server_info.delay);
    
    if(write(sock, message, sizeof(message)) < 0) // send LIST request
        perror_exit("write"); 
   
    if(read(sock, &whole_len, sizeof(whole_len)) < 0)
        perror_exit("read");
   
    int pos = 1, len = 0;
    char **right_paths = new char*[whole_len+5];

    for(int i=0; i<whole_len; ++i)
    {
        int path_len;

        if(read(sock, &path_len, sizeof(path_len)) < 0)
            perror_exit("read");
        
        char *path = new char[path_len+5];
        memset(path, 0, (path_len+5)*sizeof(char));
        
        for(int j=0; j<path_len; ++j) // read path
        {
            if(read(sock, &path[j], sizeof(path[j])) < 0)
                perror_exit("read");
        }
        
        char *path_1 = new char[strlen(content_server_info.dirfilename)+5];
        
        if(!strcmp(content_server_info.dirfilename, "."))
            strcpy(path_1, "./");
        else
            sprintf(path_1, "./%s", content_server_info.dirfilename);
        
        if(!isPrefix(path_1, path))
        {
            delete[] path_1;
            continue;
        }
        
        right_paths[len++] = path; // keep paths we asked for
        delete[] path_1;
    }

    for(int i=0; i<len; ++i)
    {
        pthread_mutex_lock(&mtx);
        
        while(buffer.count >= BUFFER_SIZE)
        {
            //printf("Found Buffer full\n");
            pthread_cond_wait(&cond_non_full, &mtx);
        }
        
        buffer.data[buffer.Tend].insert(content_server_info.address, right_paths[i], content_server_info.port, content_server_info.id, i+1, len);
        buffer.Tend = (buffer.Tend + 1) % BUFFER_SIZE;
        ++buffer.count;
        
        pthread_cond_signal(&cond_non_empty);
        pthread_mutex_unlock(&mtx);
    }
    
    for(int i=0; i<len; ++i)
        delete[] right_paths[i];
    
    delete[] right_paths;
    close(sock);
    pthread_exit(NULL);
}
