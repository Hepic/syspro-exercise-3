#ifndef CONTENT_SERVER_FUNC_H
#define CONTENT_SERVER_FUNC_H

#define CLONE_SIZE 150
#define INFO_SIZE 150

struct DelayInfo
{
    int id, delay;
};

extern pthread_mutex_t mtx;
extern DelayInfo *info;
extern int info_pos, info_len;
extern const char *dirfilename;

int getIdFromMessage(const char*);
int getDelayFromMessage(const char*); 
char* getDirFilenameFromMessage(const char*, int); 
char* typeOfMessage(const char*);
void sendDirectory(const char*, const char*, int);
void sendEntity(const char*, const char*, struct dirent*, int);
void sendDirectoryLength(const char*, int);
int getDirectoryLength(const char*);
int getEntityLength(const char*, struct dirent*);
void sendFileBytes(const char*, int, int);
void *contentServerProcedure(void*);

#endif
