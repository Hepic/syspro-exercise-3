#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include "help_functions.h"
#include "contentServer_func.h"


pthread_mutex_t mtx;
DelayInfo *info;
int info_pos = 0, info_len = INFO_SIZE;
const char *dirfilename;


int getIdFromMessage(const char *str)
{
    int ret = 0, cnt = 0;
    char *temp = new char[strlen(str)+5];
    strcpy(temp, str);

    char *ptr = strtok(temp, " ");

    while(ptr != NULL)
    {
        if(++cnt == 2)
        {
            ret = convertStrToInt(ptr);
            break;
        }
        
        ptr = strtok(NULL, " ");
    }
    
    delete[] temp;
    return ret;
}

int getDelayFromMessage(const char *str)
{
    int ret = 0, cnt = 0;
    char *temp = new char[strlen(str)+5];
    strcpy(temp, str);

    char *ptr = strtok(temp, " ");

    while(ptr != NULL)
    {
        if(++cnt == 3)
        {
            ret = convertStrToInt(ptr);
            break;
        }
        
        ptr = strtok(NULL, " ");
    }
    
    delete[] temp;
    return ret;
}


char* getDirFilenameFromMessage(const char *str, int type)
{
    char *temp = new char[strlen(str)+5];
    strcpy(temp, str);

    char *ptr = strtok(temp, " ");
    ptr = strtok(NULL, " ");

    if(type == 1)
        ptr = strtok(NULL, " ");
    
    char *ret = new char[strlen(dirfilename)+strlen(ptr)+5];
    sprintf(ret, "%s/%s", dirfilename, ptr);
    
    delete[] temp;
    return ret;
}


char* typeOfMessage(const char *str)
{
    char *temp = new char[strlen(str)+5];
    strcpy(temp, str);
    
    char *ptr = strtok(temp, " ");
    
    return ptr;
}


void sendDirectory(const char *_dirfilename, const char *_dirfilename1, int sock)
{
    DIR *dir;
    struct dirent *entity;

    if(isDir(_dirfilename))
    {
        if((dir = opendir(_dirfilename)) == NULL)
            perror_exit("opendir");
    }

    else
    {
        int path_len = strlen(_dirfilename);

        if(write(sock, &path_len, sizeof(path_len)) < 0)
            perror_exit("write");
        
        for(int i=0; i<path_len; ++i)
        {
            if(write(sock, _dirfilename+i, sizeof(_dirfilename[i])) < 0)
                perror_exit("write");
        }
        
        return;
    }

    int path_len = strlen(_dirfilename1) + 1;
    char *path = new char[path_len+5];

    memset(path, 0, (path_len+5)*sizeof(char));
    sprintf(path, "%s/", _dirfilename1);
     
    if(write(sock, &path_len, sizeof(path_len)) < 0)
        perror_exit("write");
    
    for(int i=0; i<path_len; ++i)
    {
        if(write(sock, path+i, sizeof(path[i])) < 0)
            perror_exit("write");
    }
    
    delete[] path; 

    entity = readdir(dir);

    while(entity != NULL)
    {
        sendEntity(_dirfilename, _dirfilename1, entity, sock);
        entity = readdir(dir);
    }

    closedir(dir);
}


void sendEntity(const char *_dirfilename, const char *_dirfilename1, struct dirent *entity, int sock)
{
    if(entity->d_type == DT_DIR)
    {
        if(entity->d_name[0] == '.')
            return;
        
        int path_len = strlen(_dirfilename) + strlen(entity->d_name);
        char *path = new char[path_len+5];
        
        int path_len1 = strlen(_dirfilename1) + strlen(entity->d_name);
        char *path1 = new char[path_len1+5];

        sprintf(path, "%s/%s", _dirfilename, entity->d_name);
        sprintf(path1, "%s/%s", _dirfilename1, entity->d_name);

        sendDirectory(path, path1, sock);
        
        delete[] path;
        delete[] path1;
    }

    else if(entity->d_type == DT_REG)
    {
        int path_len = strlen(_dirfilename1) + strlen(entity->d_name) + 1;
        char *path = new char[path_len+5];

        sprintf(path, "%s/%s", _dirfilename1, entity->d_name);
         
        if(write(sock, &path_len, sizeof(path_len)) < 0)
            perror_exit("write");
        
        for(int i=0; i<path_len; ++i)
        {
            if(write(sock, path+i, sizeof(path[i])) < 0)
                perror_exit("write");
        }
        
        delete[] path;
    }

    else
        fprintf(stderr, "Not a file or directory\n");
}


void sendDirectoryLength(const char *_dirfilename, int sock)
{
    int len = getDirectoryLength(_dirfilename);
    
    if(write(sock, &len, sizeof(len)) < 0)
        perror_exit("write");
}


int getDirectoryLength(const char *_dirfilename)
{
    DIR *dir;
    struct dirent *entity;
    int counter = 1;
    
    if(isDir(_dirfilename))
    {
        if((dir = opendir(_dirfilename)) == NULL)
            perror_exit("opendir");
    }
    else
        return counter;

    entity = readdir(dir);

    while(entity != NULL)
    {
        counter += getEntityLength(_dirfilename, entity);
        entity = readdir(dir);
    }

    closedir(dir);
    return counter;
}


int getEntityLength(const char *_dirfilename, struct dirent *entity)
{
    if(entity->d_type == DT_DIR)
    {
        if(entity->d_name[0] == '.')
            return 0;
        
        int path_len = strlen(_dirfilename) + strlen(entity->d_name);
        char *path = new char[path_len+5];

        sprintf(path, "%s/%s", _dirfilename, entity->d_name);
        return getDirectoryLength(path);
    }

    else if(entity->d_type == DT_REG)
        return 1; 

    return 0;
}


void sendFileBytes(const char *filename, int sock, int id)
{
    char message[100];
    FILE *fp = fopen(filename, "rb");
    
    for(int i=0; i<info_pos; ++i)
    {
        if(id == info[i].id)
            sleep(info[i].delay);
    }

    while(1) // read bytes from the file and write them in the socket
    {
        memset(message, 0, sizeof(message));
        int bytes_read = fread(message, sizeof(char), sizeof(message), fp); 
        
        if(write(sock, &bytes_read, sizeof(bytes_read)) < 0)
            perror_exit("write");

        if(bytes_read == 0)
            break;

        char *ptr = message;
        
        while(bytes_read > 0)
        {
            int bytes_written;
            
            if((bytes_written = write(sock, ptr, bytes_read)) < 0)
                perror_exit("write");
            
            bytes_read -= bytes_written;
            ptr += bytes_written;
        }
    }

    fclose(fp);
}


void *contentServerProcedure(void *args)
{
    char message[100];
    int sock = *((int*)args); 
    
    memset(message, 0, sizeof(message));
    
    if(read(sock, message, sizeof(message)) < 0)
        perror_exit("read");
    
    char *type_message = typeOfMessage(message);

    if(!strcmp(type_message, "LIST"))
    {
        int id = getIdFromMessage(message); 
        int delay = getDelayFromMessage(message);
        
        pthread_mutex_lock(&mtx);

        if(info_pos == 0)
        {
            if((info = (DelayInfo*)malloc(INFO_SIZE*sizeof(DelayInfo))) == NULL)
                perror_exit("malloc");
        }

        // for each id keep its delay
        info[info_pos].id = id;
        info[info_pos++].delay = delay;
        
        if(info_pos == info_len)
        {
            info_len += INFO_SIZE;
            info = (DelayInfo*)realloc(info, info_len*sizeof(DelayInfo));
        }
        
        pthread_mutex_unlock(&mtx);

        sendDirectoryLength(dirfilename, sock);
        sendDirectory(dirfilename, ".", sock);
    }
    
    else if(!strcmp(type_message, "FETCH"))
    {
        int id = getIdFromMessage(message);
        char *filename = getDirFilenameFromMessage(message, 1);
        sendFileBytes(filename, sock, id); 
        delete[] filename;
    }
    
    else if(!strcmp(type_message, "DIR"))
    {
        char *_dirfilename = getDirFilenameFromMessage(message, 0);
        memset(message, 0, sizeof(message));

        if(isDir(_dirfilename))
            strcpy(message, "yes");
        else
            strcpy(message, "no");
        
        if(write(sock, message, sizeof(message)) < 0) // send response
            perror_exit("write");
        
        delete[] _dirfilename;
    }

    close(sock);
    pthread_exit(NULL);
}
