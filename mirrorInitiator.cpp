#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "help_functions.h"


int main(int argc, char *argv[])
{
    const char *mirror_server_address = parseWithFlag(argc, argv, "-n");
    const char *mirror_server_port_str = parseWithFlag(argc, argv, "-p"); 
    const char *content_servers_info_str = parseWithFlag(argc, argv, "-s"); 
    int sock, str_len;
    int mirror_server_port = convertStrToInt(mirror_server_port_str);
    long int filesTransfered;
    double variance = 0, bytesTransfered, bytes, average;
    sockaddr_in server;
    sockaddr *serverptr = (sockaddr*)&server;
    hostent *rem; 
    
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");
    
    if((rem = gethostbyname(mirror_server_address)) == NULL)
        perror_exit("gethostbyname");
    
    server.sin_family = AF_INET;
    memcpy(&server.sin_addr, rem->h_addr, rem->h_length);
    server.sin_port = htons(mirror_server_port);
    
    if(connect(sock, serverptr, sizeof(server)) < 0)
        perror_exit("connect");
    
    printf("Successfull connection to %s:%d\n", mirror_server_address, mirror_server_port);
    str_len = strlen(content_servers_info_str); 
    
    if(write(sock, &str_len, sizeof(str_len)) < 0) // send length of the message
        perror_exit("write");

    for(int i=0; i<strlen(content_servers_info_str); ++i) // send message(information about contentServers)
    {
        if(write(sock, content_servers_info_str+i, 1) < 0)
            perror_exit("write");
    }
    
    // read statistics from mirrorServer
    if(read(sock, &filesTransfered, sizeof(filesTransfered)) < 0)
        perror("read");
   
    if(read(sock, &bytesTransfered, sizeof(bytesTransfered)) < 0)
        perror("read");
    
    average = bytesTransfered / (double)filesTransfered;

    for(int i=0; i<filesTransfered; ++i)
    {
        if(read(sock, &bytes, sizeof(bytes)) < 0)
            perror_exit("read");
        
        variance += (1.0/filesTransfered) * (double)(bytes-average) * (double)(bytes-average);
    }

    printf("FilesTransfered = %ld, BytesTransfered = %.2f\n", filesTransfered, bytesTransfered);
    printf("Average = %.2f\nVariance = %.2f\n", average, variance);
    
    close(sock);
    return 0;
}
