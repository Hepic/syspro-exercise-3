#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include "help_functions.h"


const char* parseWithFlag(int len, char *str[], const char *flag)
{
    for(int i=0; i<len; ++i)
        if(!strcmp(str[i], flag))
            return str[i+1];
    
    return NULL;
}


int convertStrToInt(const char *str)
{
    if(str == NULL)
        return 0;

    int ret = 0;

    while(*str != '\0')
        ret = 10*ret + *str++ - '0';

    return ret;
}


void perror_exit(const char *error)
{
    perror(error);
    exit(EXIT_FAILURE);
}


void perror_pthread_exit(const char *str, int err)
{
    fprintf(stderr, "%s:%s\n", str, strerror(err));
}


bool isRegFile(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}


bool isDir(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}


bool isPrefix(const char *str_1, const char *str_2)
{
    int len_1 = strlen(str_1);
    int len_2 = strlen(str_2);
    int pos = 0;

    while(pos < len_1  &&  pos < len_2)
    {
        if(str_1[pos] != str_2[pos])
            break;

        ++pos;
    }

    if(pos == len_1)
        return true;

    return false;
}


double getFileSize(const char *filename)
{
    struct stat stat_buf;
    int rc = stat(filename, &stat_buf);
    return rc == 0 ? (double)stat_buf.st_size : -1;
}
