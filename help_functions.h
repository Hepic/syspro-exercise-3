#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

const char* parseWithFlag(int, char*[], const char*);
int convertStrToInt(const char*);
void perror_exit(const char*);
void perror_pthread_exit(const char*, int);
bool isRegFile(const char*);
bool isDir(const char*);
bool isPrefix(const char*, const char*);
double getFileSize(const char*);

#endif
