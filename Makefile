CC = g++
FLAGS = -g -c
OUT1 = MirrorInitiator
OUT2 = MirrorServer
OUT3 = ContentServer
OBJS1 = mirrorInitiator.o help_functions.o
OBJS2 = mirrorServer.o mirrorServer_func.o help_functions.o
OBJS3 = contentServer.o contentServer_func.o help_functions.o

MirrorInitiator: $(OBJS1)
	$(CC) -g $(OBJS1) -o $(OUT1)

MirrorServer: $(OBJS2)
	$(CC) -g $(OBJS2) -o $(OUT2) -pthread

ContentServer: $(OBJS3)
	$(CC) -g $(OBJS3) -o $(OUT3) -pthread

mirrorInitiator.o: mirrorInitiator.cpp
	$(CC) $(FLAGS) mirrorInitiator.cpp

mirrorServer.o: mirrorServer.cpp
	$(CC) $(FLAGS) mirrorServer.cpp

contentServer.o: contentServer.cpp
	$(CC) $(FLAGS) contentServer.cpp

mirrorServer_func.o: mirrorServer_func.cpp
	$(CC) $(FLAGS) mirrorServer_func.cpp

contentServer_func.o: contentServer_func.cpp
	$(CC) $(FLAGS) contentServer_func.cpp

help_functions.o: help_functions.cpp
	$(CC) $(FLAGS) help_functions.cpp

clean:
	rm -f $(OBJS1) $(OBJS2) $(OBJS3) $(OUT1) $(OUT2) $(OUT3) 
