#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <pthread.h>
#include "help_functions.h"
#include "mirrorServer_func.h"


int main(int argc, char *argv[])
{
    const char *mirror_server_port_str = parseWithFlag(argc, argv, "-p"); 
    dirname = parseWithFlag(argc, argv, "-m");
    const char *thread_num_str = parseWithFlag(argc, argv, "-w");
    char *content_servers_info_str, *path;
    char temp;
    int sock, new_sock, str_len, content_servers_len, str_pos, err, path_len, content_server_id = 0;
    int mirror_server_port = convertStrToInt(mirror_server_port_str);
    int thread_num = convertStrToInt(thread_num_str);
    sockaddr_in server, client;
    sockaddr *serverptr = (sockaddr*)&server;
    sockaddr *clientptr = (sockaddr*)&client;
    hostent *rem;
    socklen_t client_len = sizeof(client);
    ContentServersInfo *content_servers_info; 
    pthread_mutex_t mtx;
    pthread_cond_t cond_non_empty, cond_non_full;
    pthread_t *mirror_managers, *workers; 
    
    if(mkdir(dirname, 0777) < 0)
        perror_exit("mkdir"); 
    
    if((workers = (pthread_t*)malloc((thread_num+5)*sizeof(pthread_t))) == NULL)
        perror_exit("malloc");

    for(int i=0; i<thread_num; ++i) // start workers
    {
        if(err = pthread_create(workers+i, NULL, workersProcedure, NULL))
            perror_pthread_exit("pthread_create", err);
    }

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        perror_exit("socket");
    
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(mirror_server_port);

    if(bind(sock, serverptr, sizeof(server)) < 0)
        perror_exit("bind");

    if(listen(sock, 5) < 0)
        perror_exit("listen");
    
    while(1)
    {
        if((new_sock = accept(sock, clientptr, &client_len)) < 0)
            perror_exit("accept");
        
        client_len = sizeof(client.sin_addr.s_addr);
        
        if((rem = gethostbyaddr((char*)&client.sin_addr.s_addr, client_len, client.sin_family)) == NULL)
            perror_exit("gethostbyaddr");

        printf("New connection from %s\n", rem->h_name);
        
        if(read(new_sock, &str_len, sizeof(str_len)) < 0) // get length of the message(information about contentServers)
            perror_exit("read");
        
        content_servers_info_str = new char[str_len+5];
        memset(content_servers_info_str, 0, sizeof(char)*(str_len+5));
        
        // initialize values
        str_pos = filesTransfered = bytesTransfered = numDevicesDone = 0; 
        pthread_mutex_init(&mtx, 0);
        pthread_mutex_init(&mtx_filesTransfered, 0);
        pthread_mutex_init(&mtx_bytesTransfered, 0);
        pthread_mutex_init(&mtx_numDevicesDone, 0);
        pthread_cond_init(&cond_non_empty, 0);
        pthread_cond_init(&cond_non_full, 0);
        pthread_cond_init(&cond_all_done, 0);
        
        for(int i=0; i<str_len; ++i)
        {
            if(read(new_sock, &temp, 1) < 0)
                perror_exit("read");

            content_servers_info_str[str_pos++] = temp; // read whole message
        }
        
        content_servers_len = getContentServersLength(content_servers_info_str);
        content_servers_info = parseContentServersInfo(content_servers_info_str, content_servers_len); 

        for(int i=0; i<content_servers_len; ++i) // create subfolders for each content server
        {
            path_len = strlen(dirname) + strlen(content_servers_info[i].address) + 10;
            
            path = new char[path_len+5];
            memset(path, 0, (path_len+5)*sizeof(char));
            sprintf(path, "%s/%s_%d", dirname, content_servers_info[i].address, content_servers_info[i].port);
            
            if(!isDir(path)  &&  mkdir(path, 0777) < 0)
                perror_exit("mkdir");

            delete[] path;
        }

        if((mirror_managers = (pthread_t*)malloc((content_servers_len+5)*sizeof(pthread_t))) == NULL)
            perror_exit("malloc");

        for(int i=0; i<content_servers_len; ++i) // start mirrorManagers
        {
            content_servers_info[i].id = content_server_id++;

            if(err = pthread_create(mirror_managers+i, NULL, mirrorManagerProcedure, (void*)(content_servers_info+i)))
                perror_pthread_exit("pthread_create", err);
        }
        
        for(int i=0; i<content_servers_len; ++i)
        {
            if(err = pthread_join(mirror_managers[i], NULL))
                perror_pthread_exit("pthread_join", err);
        }
        
        pthread_mutex_lock(&mtx_numDevicesDone);

        while(numDevicesDone < content_servers_len)
            pthread_cond_wait(&cond_all_done, &mtx_numDevicesDone);
        
        pthread_mutex_unlock(&mtx_numDevicesDone);
        printf("FilesTransfered = %ld, BytesTransfered = %.2f, numDeviceDone = %ld\n", filesTransfered, bytesTransfered, numDevicesDone);
        
        if(write(new_sock, &filesTransfered, sizeof(filesTransfered)) < 0)
            perror_exit("write");
        
        if(write(new_sock, &bytesTransfered, sizeof(bytesTransfered)) < 0)
            perror_exit("write");
        
        content_servers_len = getContentServersLength(content_servers_info_str);
        content_servers_info = parseContentServersInfo(content_servers_info_str, content_servers_len);
        
        for(int i=0; i<content_servers_len; ++i)
        {
            path_len = strlen(dirname) + strlen(content_servers_info[i].address) + 10;
            
            path = new char[path_len+5];
            memset(path, 0, (path_len+5)*sizeof(char));
            sprintf(path, "%s/%s_%d", dirname, content_servers_info[i].address, content_servers_info[i].port);
            
            sendLengthBytesDir(path, new_sock);
            delete[] path;
        }

        delete[] content_servers_info_str;
        close(new_sock);
    }

    for(int i=0; i<thread_num; ++i)
    {
        if(err = pthread_join(workers[i], NULL))
            perror_pthread_exit("pthread_join", err);
    }
    
    close(sock);
    return 0;
}

